<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
}); 
// Route::get('authors', 'AuthorsController@index');
// Route::get('authors/{author}', 'AuthorsController@show'); 
Route::post('fetchSignup','AuthorsController@fetchSignup');
Route::post('fetchSignin','AuthorsController@fetchSignin');
// Route::put('authors/{author}','AuthorsController@update');
// Route::delete('authors/{author}', 'AuthorsController@destroy');


// Route::get('authorsBooks', 'AuthorsBookController@index');
// Route::get('authorsBooks/{authorsBook}', 'AuthorsBookController@show');
// Route::post('authorsBooks','AuthorsBookController@store');
// Route::put('authorsBooks/{authorsBook}','AuthorsBookController@update');
// Route::delete('authorsBooks/{authorsBook}', 'AuthorsBookController@destroy');


Route::get('fetchBooks', 'BookController@index');
Route::get('books/{book}', 'BookController@show');
Route::post('books','BookController@store');
Route::post('fetchNewBook','BookController@fetchNewBook');
Route::put('books/{book}','BookController@update');
Route::delete('books/{book}', 'BookController@destroy');