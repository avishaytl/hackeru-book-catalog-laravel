<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Books;

class BookController extends Controller
{
    public function index()
    { 
        return response()->json(['success'=> true, Books::all()]); 
    } 
    public function create()
    {
        //
    } 
    public function store(Request $request)
    { 
        $book = Books::create($request->all()); 
        return response()->json($book, 201); 
    } 
    public function show(Books $book)
    {
        return $book;
    } 
    public function edit($id)
    {
        //
    } 
    public function fetchNewBook(Request $request){ 
        $name = $request->input('book_name');
        $description = $request->input('book_description');
        $status = 1;//$request->input('book_description');
        $name = strtolower($name);
        if ($request->hasFile('file')) {
            $image = $request->file('file'); 
            $destinationPath = 'C:/xampp/htdocs/book_catalog_laravel/public/images/';
            $data = array(
                'name' => $name,
                'description' => $description,
                'url_img' => $destinationPath . $name.'.'.$image->getClientOriginalExtension(),
                'status' => $status,
            );
            $res = Books::create($data);
            if(!$res)
                return response()->json(['success'=> false]);
            $image = move_uploaded_file($image, $destinationPath .'/'. $name.'.'.$image->getClientOriginalExtension()); 
        }
        return back()->with('success');
        // return response()->json(['success'=> true]);    
    }
    public function update(Books $book)
    {
        $book->update($request->all()); 
        return response()->json($product, 200);
    } 
    public function destroy(Books $book)
    { 
        $book->delete(); 
        return response()->json(null, 204);
    }
}
