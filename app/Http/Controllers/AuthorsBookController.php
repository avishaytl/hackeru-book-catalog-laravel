<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\AuthorsBook;

class AuthorsBookController extends Controller
{
    public function index()
    {
        return AuthorsBook::all();
    } 
    public function create()
    {
        //
    } 
    public function store(Request $request)
    { 
        $author = AuthorsBook::create($request->all()); 
        return response()->json($author, 201); 
    } 
    public function show(AuthorsBook $author)
    {
        return $author;
    } 
    public function edit($id)
    {
        //
    } 
    public function update(AuthorsBook $author)
    {
        $author->update($request->all()); 
        return response()->json($product, 200);
    } 
    public function destroy(AuthorsBook $author)
    { 
        $author->delete(); 
        return response()->json(null, 204);
    }
}
