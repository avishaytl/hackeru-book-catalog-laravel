<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Authors;
class AuthorsController extends Controller
{ 
    public function index()
    { 
        return Authors::all();
    } 
    public function create()
    {
        //
    } 
    public function fetchSignup(Request $request)
    {  
        $author = Authors::create($request->all()); 
        return response()->json(['success'=> true]); 
    }
    public function fetchSignin(Request $request)
    {  
        $email = $request->input('email');
        $pass = $request->input('email');
        $name = Authors::select('first_name')->where('email',$email)->where('password',$pass)->get(); 
        $id = Authors::select('id')->where('email',$email)->where('password',$pass)->get(); 
        if(!empty($name) && !empty($id)){ 
            return response()->json(['success'=> true,'name'=>$name[0],'id'=>$id[0]]); 
        }
        else
            return response()->json(['success'=> false]); 
    } 
    public function show(Authors $author)
    {
        return $author;
    } 
    public function edit($id)
    {
        //
    } 
    public function update(Authors $author)
    {
        $author->update($request->all()); 
        return response()->json($product, 200);
    } 
    public function destroy(Authors $author)
    { 
        $author->delete(); 
        return response()->json(null, 204);
    }
}
