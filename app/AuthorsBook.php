<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AuthorsBook extends Model
{
    protected $fillable =['user_id','book_id'];
}
