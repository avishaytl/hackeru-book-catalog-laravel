<?php

use Illuminate\Database\Seeder;
use App\Authors;
class AuthorsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = \Faker\Factory::create(); 
        for ($i = 0; $i < 2; $i++) {
            Authors::create([
                'first_name' => $faker->title,
                'last_name' => $faker->title,
                'email' => $faker->title,
                'phone' => $faker->title,
                'password' => $faker->title,
                'status' => 1
            ]);
        }
    }
}
